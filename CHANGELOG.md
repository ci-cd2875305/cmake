# Changelog

## 3.29.1 (2024-04-06)

#### New Features

* :sparkles: cmake 2.29.1

Full set of changes: [`3.28.2...3.29.1`](https://gitlab.com/ci-cd2875305/cmake/compare/3.28.2...3.29.1)

## 3.28.2 (2024-02-20)

#### New Features

* :sparkles: cmake 3.28.2

Full set of changes: [`3.23.1...3.28.2`](https://gitlab.com/ci-cd2875305/cmake/compare/3.23.1...3.28.2)

## 3.23.1 (2022-04-25)

#### New Features

* :sparkles: cmake 3.23.1
#### Others

* :green_heart: YAML template
