FROM gcc:13.2.0

ARG CMAKE_VERSION=3.29.1

ADD install.sh /

RUN chmod +x /install.sh
RUN /install.sh

CMD ["/bin/bash"]