#!/bin/sh

TEMP_FOLDER="tmp"

apt update && apt upgrade -y

cd $TEMP_FOLDER
wget https://github.com/Kitware/CMake/releases/download/v$CMAKE_VERSION/cmake-$CMAKE_VERSION.tar.gz
tar xvf cmake-$CMAKE_VERSION.tar.gz
cd cmake-$CMAKE_VERSION
./bootstrap && make && make install
cd ../..
apt-get update && apt-get install -y libgtest-dev libboost-test-dev && rm -rf /var/lib/apt/lists/*
rm -rf $TEMP_FOLDER/*

#SELF REMOVED
rm -- "$0"
